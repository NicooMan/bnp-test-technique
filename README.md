# Test Technique BNP Nicolas NGUYEN-KHOA-MAN

Test technique pour un stage de Data Scientist à la BNP PF.
<br>Le .zip doit contenir:
- price_prediction.ipynb : jupyter notebook pour le cas usage (partie 3)
- train.csv : données pour le cas d'usage
- Test_BNP_Nicolas_NGUYEN-KHOA-MAN.pdf : réponses aux questions du test (partie 1 et 2)
- requirements.txt : dépendances du notebook python
- README.md

Pour installer les dépendances, utiliser la commande:
`pip install -r requirements.txt`
